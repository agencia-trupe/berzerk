-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2.2
-- http://www.phpmyadmin.net
--
-- Servidor: 179.188.16.86
-- Tempo de Geração: 27/11/2015 às 10:33:37
-- Versão do Servidor: 5.6.21
-- Versão do PHP: 5.3.3-7+squeeze27

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `trupe1104`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `link`, `created_at`, `updated_at`) VALUES
(2, 0, '20151125192923_banner-home-hdoke.png', 'http://trupe.net/previa-berzek/produtos/novo', '2015-11-25 21:29:24', '2015-11-25 21:29:24');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE IF NOT EXISTS `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `googleplus` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `telefone`, `email`, `endereco`, `googlemaps`, `facebook`, `googleplus`, `instagram`, `twitter`, `youtube`, `created_at`, `updated_at`) VALUES
(1, '11 2916 8170', 'contato@berzek.com.br', 'Rua Barcelos Leite, 193 - Vila Primavera - São Paulo, SP - 03390-040', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.349791591107!2d-46.52650494864916!3d-23.591784968528163!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5d7a39ed0305%3A0xbc5dd8497ad1a4b2!2sR.+Barcelos+Leite%2C+193+-+Vila+Primavera%2C+S%C3%A3o+Paulo+-+SP%2C+03390-040!5e0!3m2!1spt-BR!2sbr!4v1447942948495" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 'https://www.facebook.com/BerzekEletronicaLtda', '', '', '', '', '0000-00-00 00:00:00', '2015-11-24 16:47:46');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE IF NOT EXISTS `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `assunto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contatos_recebidos`
--

INSERT INTO `contatos_recebidos` (`id`, `assunto`, `nome`, `email`, `mensagem`, `lido`, `created_at`, `updated_at`) VALUES
(1, 'Compras ou informações', 'Teste', 'teste@teste.com', 'mensagem.', 0, '2015-11-25 16:12:08', '2015-11-25 16:12:08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `downloads`
--

CREATE TABLE IF NOT EXISTS `downloads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `downloads_categoria_id` int(10) unsigned DEFAULT NULL,
  `produto_id` int(10) unsigned DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `downloads_downloads_categoria_id_foreign` (`downloads_categoria_id`),
  KEY `downloads_produto_id_foreign` (`produto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `downloads`
--

INSERT INTO `downloads` (`id`, `downloads_categoria_id`, `produto_id`, `ordem`, `titulo`, `slug`, `descricao`, `arquivo`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, 'Manual de instalação - Amplificadores', '', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. ', '20151125193352_apresentacao institucional potenza.pdf', '2015-11-25 21:33:31', '2015-11-25 21:33:52'),
(2, 1, 2, 0, 'Pra ver como fica com mais de um cadastro', '', 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. ', '20151125193434_apresentacao institucional potenza.pdf', '2015-11-25 21:34:34', '2015-11-25 21:34:34');

-- --------------------------------------------------------

--
-- Estrutura da tabela `downloads_categorias`
--

CREATE TABLE IF NOT EXISTS `downloads_categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `downloads_categorias`
--

INSERT INTO `downloads_categorias` (`id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 0, 'Manuais', 'manuais', '2015-11-24 16:44:19', '2015-11-24 16:44:19');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_11_19_125206_create_banners_table', 1),
('2015_11_19_125444_create_produtos_categorias_table', 1),
('2015_11_19_125503_create_produtos_table', 1),
('2015_11_19_125552_create_produtos_imagens_table', 1),
('2015_11_19_125619_create_servicos_table', 1),
('2015_11_19_125638_create_downloads_categorias_table', 1),
('2015_11_19_125648_create_downloads_table', 1),
('2015_11_19_125708_create_contato_table', 1),
('2015_11_19_125719_create_contatos_recebidos_table', 1),
('2015_11_19_131908_create_newsletter_table', 1),
('2015_11_19_145920_create_novo_table', 1),
('2015_11_19_150003_create_novo_imagens_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `newsletter`
--

CREATE TABLE IF NOT EXISTS `newsletter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `newsletter_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `novo`
--

CREATE TABLE IF NOT EXISTS `novo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` text COLLATE utf8_unicode_ci NOT NULL,
  `especificacoes` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `manual` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `novo`
--

INSERT INTO `novo` (`id`, `titulo`, `subtitulo`, `especificacoes`, `imagem`, `manual`, `video`, `created_at`, `updated_at`) VALUES
(1, '<p>HD-OK&Ecirc;!!</p>\r\n\r\n<p>Um Karaok&ecirc; profissional</p>\r\n\r\n<p><strong>como voc&ecirc; nunca viu!</strong></p>\r\n', '<p>O <strong>HD-OK&Ecirc;!!</strong> trabalha com o que h&aacute; de mais moderno no mercado mundial de karaok&ecirc;s.</p>\r\n\r\n<p>Ele reproduz v&iacute;deos em alta defini&ccedil;&atilde;o, trabalha com reserva de at&eacute; 100 m&uacute;sicas, possui atualiza&ccedil;&otilde;es trimestais e muito mais!</p>\r\n', '<p>Equipamento para reprodu&ccedil;&atilde;o de arquivos multi-m&iacute;dia, fun&ccedil;&otilde;es de Jukebox ,reprodutor de karaok&ecirc; e video-clips:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Caracter&iacute;sticas:</strong></p>\r\n\r\n<ul>\r\n	<li>N&atilde;o utiliza softwares comerciais</li>\r\n	<li>Armazena at&eacute; 100.000 m&uacute;sicas com uma HDD 3T (trabalha muito bem com dispositivos SDD)</li>\r\n	<li>Proporciona pontua&ccedil;&atilde;o real e virtual</li>\r\n	<li>Reserva 100 m&uacute;sicas na tela na fun&ccedil;&atilde;o karaok&ecirc;</li>\r\n	<li>Alta defini&ccedil;&atilde;o de v&iacute;deo (DIX/XVID)</li>\r\n	<li>Teclado sem fio soft-touch retro-iluminado (opcional)</li>\r\n	<li>Reproduz v&iacute;deo-clips e m&uacute;sicas normais</li>\r\n	<li>Acompanha controle remoto e inicialmente 3700 m&uacute;sicas</li>\r\n	<li>Conte&uacute;do criptografado com seu c&oacute;digo</li>\r\n	<li>Entrada de microfones inteligentes, s&oacute; funcionam com m&uacute;sicas karaok&ecirc;</li>\r\n	<li>Mixer,eco e tonalidade nos microfones</li>\r\n	<li>Sa&iacute;da HDMI, COMPONENTE, COMPOSTO,VGA, etc...</li>\r\n	<li>Conex&atilde;o direta para 2 noteiros ou moedeiros j&aacute; com conex&atilde;o 12v</li>\r\n	<li>Atualiza&ccedil;&otilde;es bimestrais de 200 a 300 m&uacute;sicas</li>\r\n	<li>Atualiza&ccedil;&otilde;es via PENDRIVE ou NOTEBOOK</li>\r\n	<li>Playbacks e v&iacute;deos feitos e sincronizados em est&uacute;dios pr&oacute;prios</li>\r\n	<li>Auto &iacute;ndice de nacionaliza&ccedil;&atilde;o na eletr&ocirc;nica, mais confian&ccedil;a na manuten&ccedil;&atilde;o</li>\r\n	<li>Possibilidade de propaganda pessoal ou de sua empresa na tela inicial</li>\r\n	<li>Escolha de m&uacute;sicas em atualiza&ccedil;&otilde;es por profissionais do ramo, billboard e playlists de r&aacute;dios</li>\r\n	<li>V&iacute;deos tem&aacute;ticos nas fun&ccedil;&atilde;o karaok&ecirc;</li>\r\n	<li>Gabinete em metal e painel adesivado em policarbonato</li>\r\n	<li>Alimenta&ccedil;&atilde;o de 100V a 240VCA autom&aacute;tico</li>\r\n</ul>\r\n', '20151124144522_hdoke2.png', '', 'X1Fr0CAeuRk', '0000-00-00 00:00:00', '2015-11-25 21:11:30');

-- --------------------------------------------------------

--
-- Estrutura da tabela `novo_imagens`
--

CREATE TABLE IF NOT EXISTS `novo_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `novo_imagens`
--

INSERT INTO `novo_imagens` (`id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 0, '20151124144717_hdoke3.png', '2015-11-24 16:47:17', '2015-11-24 16:47:17'),
(2, 0, '20151124144717_hdoke4.png', '2015-11-24 16:47:17', '2015-11-24 16:47:17');

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE IF NOT EXISTS `produtos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produtos_categoria_id` int(10) unsigned DEFAULT NULL,
  `destaque` int(11) NOT NULL DEFAULT '0',
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `produtos_produtos_categoria_id_foreign` (`produtos_categoria_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `produtos_categoria_id`, `destaque`, `ordem`, `imagem`, `titulo`, `slug`, `descricao`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, '20151125192715_destaque.png', 'Nome do produto Amplificador', 'nome-do-produto-amplificador', '<p>Teste de aplica&ccedil;&atilde;o de texto de descri&ccedil;&atilde;o do produto.</p>\r\n\r\n<ul>\r\n	<li>Colocar caracter&iacute;sticas</li>\r\n	<li>Citar vantagens</li>\r\n</ul>\r\n\r\n<p>Continuar descri&ccedil;&atilde;o do produto com diferenciais, novidades, etc.</p>\r\n\r\n<p>Como comprar.</p>\r\n', '2015-11-25 21:26:26', '2015-11-25 21:28:35'),
(2, 1, 0, 0, '20151125193048_produto2.png', 'Nome do Produto Dois', 'nome-do-produto-dois', '<p>Teste de aplica&ccedil;&atilde;o de conte&uacute;do para descri&ccedil;&atilde;o de produto.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.&nbsp;</p>\r\n', '2015-11-25 21:30:48', '2015-11-25 21:30:48');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_categorias`
--

CREATE TABLE IF NOT EXISTS `produtos_categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `produtos_categorias`
--

INSERT INTO `produtos_categorias` (`id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 0, 'Amplificadores', 'amplificadores', '2015-11-24 16:43:34', '2015-11-24 16:43:34'),
(2, 1, 'Módulos', 'modulos', '2015-11-24 16:43:39', '2015-11-24 16:43:39'),
(3, 2, 'Periféricos', 'perifericos', '2015-11-24 16:43:42', '2015-11-24 16:43:42'),
(4, 3, 'Caixas Acústicas', 'caixas-acusticas', '2015-11-24 16:43:47', '2015-11-24 16:43:47'),
(5, 4, 'Karaokê', 'karaoke', '2015-11-24 16:43:51', '2015-11-24 16:43:51'),
(6, 5, 'Acessórios', 'acessorios', '2015-11-24 16:43:55', '2015-11-24 16:43:55');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_imagens`
--

CREATE TABLE IF NOT EXISTS `produtos_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produto_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `produtos_imagens_produto_id_foreign` (`produto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `produtos_imagens`
--

INSERT INTO `produtos_imagens` (`id`, `produto_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '20151125192637_destaque.png', '2015-11-25 21:26:37', '2015-11-25 21:26:37'),
(2, 1, 1, '20151125192646_hdoke4.png', '2015-11-25 21:26:47', '2015-11-25 21:26:47'),
(3, 2, 0, '20151125193107_produto2.png', '2015-11-25 21:31:07', '2015-11-25 21:31:07');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE IF NOT EXISTS `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `ordem`, `titulo`, `subtitulo`, `descricao`, `created_at`, `updated_at`) VALUES
(1, 0, 'Nome do Serviço', 'Serviço exclusivo da Berzek', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. ', '2015-11-25 21:32:05', '2015-11-25 21:32:05'),
(2, 0, 'Mais um serviço', 'Pra ver como fica com dois cadastros', 'Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram.', '2015-11-25 21:54:25', '2015-11-25 21:54:25');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$poJwjgnN8vIBV1DhNPuTQu3WTyiy8sLChPVvlHbfuxAb8c830Z5Xi', 'AJfZFYs1YwA98mbo7kPPp2zI4orhMG7uyWCKAGv15SwaN39luxOHlUASnqfl', '0000-00-00 00:00:00', '2015-11-24 19:21:17'),
(2, 'berzek', 'contato@berzek.com.br', '$2y$10$HqQfUU806ehw0Ho9ParYduH87FVYTkB.dCsBfdkab7QJ2vf9wOBWu', NULL, '2015-11-25 20:44:32', '2015-11-25 20:44:32');

--
-- Restrições para as tabelas dumpadas
--

--
-- Restrições para a tabela `downloads`
--
ALTER TABLE `downloads`
  ADD CONSTRAINT `downloads_downloads_categoria_id_foreign` FOREIGN KEY (`downloads_categoria_id`) REFERENCES `downloads_categorias` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `downloads_produto_id_foreign` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`) ON DELETE SET NULL;

--
-- Restrições para a tabela `produtos`
--
ALTER TABLE `produtos`
  ADD CONSTRAINT `produtos_produtos_categoria_id_foreign` FOREIGN KEY (`produtos_categoria_id`) REFERENCES `produtos_categorias` (`id`) ON DELETE SET NULL;

--
-- Restrições para a tabela `produtos_imagens`
--
ALTER TABLE `produtos_imagens`
  ADD CONSTRAINT `produtos_imagens_produto_id_foreign` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
