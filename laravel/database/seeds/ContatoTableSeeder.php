<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'telefone'   => '11 2916 8170',
            'email'      => 'contato@berzek.com.br',
            'endereco'   => 'Rua Barcelos Leite, 193 - Vila Primavera - São Paulo, SP - 03390-040',
            'googlemaps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.349791591107!2d-46.52650494864916!3d-23.591784968528163!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5d7a39ed0305%3A0xbc5dd8497ad1a4b2!2sR.+Barcelos+Leite%2C+193+-+Vila+Primavera%2C+S%C3%A3o+Paulo+-+SP%2C+03390-040!5e0!3m2!1spt-BR!2sbr!4v1447942948495" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>'
        ]);
    }
}
