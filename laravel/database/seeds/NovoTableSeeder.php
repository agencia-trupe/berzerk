<?php

use Illuminate\Database\Seeder;

class NovoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('novo')->insert([
            'titulo'         => 'Título',
            'subtitulo'      => 'Subtítulo',
            'especificacoes' => 'Especificações',
        ]);
    }
}
