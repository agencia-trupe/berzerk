<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNovoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('novo', function (Blueprint $table) {
            $table->increments('id');
            $table->text('titulo');
            $table->text('subtitulo');
            $table->text('especificacoes');
            $table->string('imagem');
            $table->string('manual');
            $table->string('video');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('novo');
    }
}
