<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDownloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('downloads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('downloads_categoria_id')->unsigned()->nullable();
            $table->integer('produto_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->text('descricao');
            $table->string('arquivo');
            $table->timestamps();

            $table->foreign('downloads_categoria_id')->references('id')->on('downloads_categorias')->onDelete('set null');
            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('downloads');
    }
}
