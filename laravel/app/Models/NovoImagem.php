<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NovoImagem extends Model
{
    protected $table = 'novo_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
