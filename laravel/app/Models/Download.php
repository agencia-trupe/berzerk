<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
    protected $table = 'downloads';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('downloads_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\DownloadCategoria', 'downloads_categoria_id');
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', "%$termo%")
                   ->orWhere('descricao', 'LIKE', "%$termo%");
    }
}
