<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Novo extends Model
{
    protected $table = 'novo';

    protected $guarded = ['id'];
}
