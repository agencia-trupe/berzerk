<?php

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('produtos/novo', ['as' => 'produtos.novo', 'uses' => 'ProdutosController@novo']);
Route::get('produtos/{produtos_categoria_slug?}', ['as' => 'produtos.index', 'uses' => 'ProdutosController@index']);
Route::get('produtos/{produtos_categoria_slug}/{produto_slug}', ['as' => 'produtos.show', 'uses' => 'ProdutosController@show']);
Route::get('servicos', ['as' => 'servicos', 'uses' => 'ServicosController@index']);
Route::get('downloads/{downloads_categoria_slug?}', ['as' => 'downloads', 'uses' => 'DownloadsController@index']);
Route::get('busca', ['as' => 'busca', 'uses' => 'HomeController@busca']);
Route::get('contato', ['as' => 'contato', 'uses' => 'ContatoController@index']);
Route::post('contato', ['as' => 'contato.envio', 'uses' => 'ContatoController@envio']);
Route::post('newsletter', ['as' => 'newsletter', 'uses' => 'HomeController@newsletter']);


// Painel
Route::group(['prefix' => 'painel', 'namespace' => 'Painel', 'middleware' => 'auth'], function() {
    Route::get('/', ['as' => 'painel', 'uses' => 'HomeController@index']);
    Route::resource('banners', 'BannersController');
    Route::get('produtos/select/{categorias}', 'ProdutosController@produtos');
    Route::resource('produtos/destaques', 'ProdutosDestaquesController');
    Route::resource('produtos/categorias', 'ProdutosCategoriasController');
    Route::resource('produtos', 'ProdutosController');
    Route::resource('produtos.imagens', 'ProdutosImagensController');
    Route::resource('servicos', 'ServicosController');
    Route::resource('downloads/categorias', 'DownloadsCategoriasController');
    Route::resource('downloads', 'DownloadsController');
    Route::resource('novo/imagens', 'NovoImagensController');
    Route::resource('novo', 'NovoController');
    Route::resource('contato/recebidos', 'ContatosRecebidosController');
    Route::resource('contato', 'ContatoController');
    Route::get('newsletter/exportar', ['as' => 'painel.newsletter.exportar', 'uses' => 'NewsletterController@exportar']);
    Route::resource('newsletter', 'NewsletterController');
    Route::resource('usuarios', 'UsuariosController');
    Route::post('order', 'HomeController@order');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});


// Auth
Route::group(['prefix' => 'painel', 'namespace' => 'Auth'], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'auth', 'uses' => 'AuthController@postLogin']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
});
