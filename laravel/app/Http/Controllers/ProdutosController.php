<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ProdutoCategoria;
use App\Models\Produto;

use App\Models\Novo;
use App\Models\NovoImagem;

class ProdutosController extends Controller
{
    public function index(ProdutoCategoria $categoria)
    {
        if (! $categoria->exists) {
            $categoria = ProdutoCategoria::ordenados()->first() ?: \App::abort('404');
        }

        $produtos = $categoria->produtos()->paginate(9);

        return view('frontend.produtos.index', compact('categoria', 'produtos'));
    }

    public function show(ProdutoCategoria $categoria, Produto $produto)
    {
        return view('frontend.produtos.show', compact('categoria', 'produto'));
    }

    public function novo()
    {
        $novo    = Novo::first();
        $imagens = NovoImagem::ordenados()->get();

        return view('frontend.produtos.novo', compact('novo', 'imagens'));
    }
}
