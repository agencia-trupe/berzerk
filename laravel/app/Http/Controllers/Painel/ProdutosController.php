<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\ProdutosRequest;

use App\Models\ProdutoCategoria;
use App\Models\Produto;
use App\Helpers\CropImage;

class ProdutosController extends Controller
{
    private $categorias;

    private $image_config = [
        [
            'width'   => 275,
            'height'  => 200,
            'produto' => true,
            'path'    => 'assets/img/produtos/'
        ],
        [
            'width'   => 180,
            'height'  => 180,
            'produto' => true,
            'path'    => 'assets/img/produtos/thumbs/'
        ]
    ];

    public function __construct()
    {
        $this->categorias = ProdutoCategoria::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (ProdutoCategoria::find($filtro)) {
            $produtos = Produto::ordenados()->categoria($filtro)->get();
        } else {
            $produtos = Produto::join('produtos_categorias as cat', 'cat.id', '=', 'produtos_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('produtos.*')
                ->ordenados()->get();
        }

        return view('painel.produtos.index', compact('categorias', 'produtos', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.produtos.create', compact('categorias'));
    }

    public function store(ProdutosRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Produto::create($input);
            return redirect()->route('painel.produtos.index')->with('success', 'Produto adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar produto: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $produto)
    {
        $categorias = $this->categorias;

        return view('painel.produtos.edit', compact('categorias', 'produto'));
    }

    public function update(ProdutosRequest $request, Produto $produto)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $produto->update($input);
            return redirect()->route('painel.produtos.index')->with('success', 'Produto alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar produto: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto)
    {
        try {

            $produto->delete();
            return redirect()->route('painel.produtos.index')->with('success', 'Produto excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir produto: '.$e->getMessage()]);

        }
    }

    public function produtos(ProdutoCategoria $categoria)
    {
        return $categoria->produtos()->where('destaque', 0)->orderBy('titulo', 'ASC')->lists('titulo', 'id')->toJson();
    }
}
