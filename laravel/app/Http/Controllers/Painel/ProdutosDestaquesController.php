<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosDestaquesRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\ProdutoCategoria;

class ProdutosDestaquesController extends Controller
{
    private $limite = 6;

    public function index()
    {
        $destaques = Produto::ordenados()->destaque()->get();
        $limite    = $this->limite;

        return view('painel.produtos.destaques.index', compact('destaques', 'limite'));
    }

    public function create()
    {
        $categorias = ProdutoCategoria::ordenados()->lists('titulo', 'id');

        return view('painel.produtos.destaques.create', compact('categorias'));
    }

    public function store(ProdutosDestaquesRequest $request)
    {
        if (count(Produto::destaque()->get()) >= $this->limite) {
            return redirect()->route('painel.produtos.destaques.index')->withErrors(['Número máximo de destaques atingido.']);
        }

        $produto = Produto::find($request->get('produto_id'));
        if (! $produto) abort('404');

        try {

            $produto->destaque = 1;
            $produto->save();

            return redirect()->route('painel.produtos.destaques.index')->with('success', 'Destaque adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar destaque: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto)
    {
        try {

            $produto->destaque = 0;
            $produto->save();
            return redirect()->route('painel.produtos.destaques.index')->with('success', 'Destaque excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir destaque: '.$e->getMessage()]);

        }
    }
}
