<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ServicosRequest;
use App\Http\Controllers\Controller;

use App\Models\Servico;

class ServicosController extends Controller
{
    public function index()
    {
        $servicos = Servico::ordenados()->get();

        return view('painel.servicos.index', compact('servicos'));
    }

    public function create()
    {
        return view('painel.servicos.create');
    }

    public function store(ServicosRequest $request)
    {
        try {

            $input = $request->all();

            Servico::create($input);
            return redirect()->route('painel.servicos.index')->with('success', 'Serviço adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar serviço: '.$e->getMessage()]);

        }
    }

    public function edit(Servico $servico)
    {
        return view('painel.servicos.edit', compact('servico'));
    }

    public function update(ServicosRequest $request, Servico $servico)
    {
        try {

            $input = $request->all();

            $servico->update($input);
            return redirect()->route('painel.servicos.index')->with('success', 'Serviço alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar serviço: '.$e->getMessage()]);

        }
    }

    public function destroy(Servico $servico)
    {
        try {

            $servico->delete();
            return redirect()->route('painel.servicos.index')->with('success', 'Serviço excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir serviço: '.$e->getMessage()]);

        }
    }
}
