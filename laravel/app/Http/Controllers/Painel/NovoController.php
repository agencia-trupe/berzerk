<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\NovoRequest;
use App\Http\Controllers\Controller;

use App\Models\Novo;
use App\Helpers\CropImage;

class NovoController extends Controller
{
    private $image_config = [
        'width'  => 290,
        'height' => null,
        'path'   => 'assets/img/novo/'
    ];

    public function index()
    {
        $novo = Novo::first();

        return view('painel.novo.index', compact('novo'));
    }

    public function update(NovoRequest $request, Novo $novo)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            if (isset($input['imagem'])) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            }

            if (isset($input['manual'])) {
                $input['manual'] = $this->uploadArquivo($input['manual']);
            }

            $novo->update($input);
            return redirect()->route('painel.novo.index')->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar página: '.$e->getMessage()]);

        }
    }

    public function uploadArquivo($file)
    {
        $filePath = 'assets/downloads/';
        $fileName = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $file->getClientOriginalName());
        $fileName = date('YmdHis').'_'.$fileName;

        $file->move(public_path($filePath), $fileName);
        return $fileName;
    }
}
