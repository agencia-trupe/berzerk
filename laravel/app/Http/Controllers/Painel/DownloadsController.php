<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DownloadsRequest;
use App\Http\Controllers\Controller;

use App\Models\DownloadCategoria;
use App\Models\Download;
use App\Models\Produto;

class DownloadsController extends Controller
{
    private $categorias;
    private $produtos;

    public function __construct()
    {
        $this->categorias = DownloadCategoria::ordenados()->lists('titulo', 'id');
        $this->produtos   = Produto::orderBy('titulo', 'ASC')->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (DownloadCategoria::find($filtro)) {
            $downloads = Download::ordenados()->categoria($filtro)->get();
        } else {
            $downloads = Download::join('downloads_categorias as cat', 'cat.id', '=', 'downloads_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('downloads.*')
                ->ordenados()->get();
        }

        return view('painel.downloads.index', compact('categorias', 'downloads', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;
        $produtos   = $this->produtos;

        return view('painel.downloads.create', compact('categorias', 'produtos'));
    }

    public function store(DownloadsRequest $request)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            $input['arquivo'] = $this->uploadArquivo($input['arquivo']);

            Download::create($input);
            return redirect()->route('painel.downloads.index')->with('success', 'Download adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar download: '.$e->getMessage()]);

        }
    }

    public function edit(Download $download)
    {
        $categorias = $this->categorias;
        $produtos   = $this->produtos;

        return view('painel.downloads.edit', compact('categorias', 'produtos', 'download'));
    }

    public function update(DownloadsRequest $request, Download $download)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            if (isset($input['arquivo'])) {
                $input['arquivo'] = $this->uploadArquivo($input['arquivo']);
            }

            if (! isset($input['produto_id'])) {
                $input['produto_id'] = null;
            }

            $download->update($input);
            return redirect()->route('painel.downloads.index')->with('success', 'Download alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar download: '.$e->getMessage()]);

        }
    }

    public function destroy(Download $download)
    {
        try {

            $download->delete();
            return redirect()->route('painel.downloads.index')->with('success', 'Download excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir download: '.$e->getMessage()]);

        }
    }

    public function uploadArquivo($file)
    {
        $filePath = 'assets/downloads/';
        $fileName = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $file->getClientOriginalName());
        $fileName = date('YmdHis').'_'.$fileName;

        $file->move(public_path($filePath), $fileName);
        return $fileName;
    }
}
