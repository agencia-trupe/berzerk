<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\NovoImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\NovoImagem;
use App\Helpers\CropImage;

class NovoImagensController extends Controller
{
    private $image_config = [
        [
            'width'  => 180,
            'height' => 180,
            'path'   => 'assets/img/novo/imagens/thumbs/'
        ],
        [
            'width'  => 980,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/novo/imagens/'
        ]
    ];

    public function index()
    {
        $imagens = NovoImagem::ordenados()->get();

        return view('painel.novo.imagens.index', compact('imagens'));
    }

    public function store(NovoImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = NovoImagem::create($input);
            $view   = view('painel.novo.imagens.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(NovoImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.novo.imagens.index')
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }
}
