<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\NewsletterRequest;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Newsletter;
use App\Models\Produto;
use App\Models\Download;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();

        return view('frontend.home', compact('banners'));
    }

    public function newsletter(NewsletterRequest $request, Newsletter $newsletter)
    {
        $newsletter->create($request->all());

        $response = [
            'status'  => 'success',
            'message' => 'cadastro efetuado com sucesso!'
        ];

        return response()->json($response);
    }

    public function busca(Request $request)
    {
        if (! $request->has('q')) return redirect()->route('home');

        $termo = $request->get('q');

        $produtos  = Produto::with('categoria')->busca($termo)->get();
        $downloads = Download::busca($termo)->get();

        return view('frontend.busca', compact('produtos', 'downloads', 'termo'));
    }
}
