<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Http\Controllers\Controller;

use App\Models\ContatoRecebido;

class ContatoController extends Controller
{
    public function index(Request $request)
    {
        $assunto = $request->get('assunto') ?: 'Compras ou informações';

        return view('frontend.contato', compact('assunto'));
    }

    public function envio(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $contatoRecebido->create($request->all());

        $contato = \App\Models\Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->from('noreply@berzek.com.br', 'Berzek')
                        ->subject('[CONTATO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => 'Mensagem enviada com sucesso!'
        ];

        return response()->json($response);
    }
}
