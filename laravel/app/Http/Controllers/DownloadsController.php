<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Download;
use App\Models\DownloadCategoria;

class DownloadsController extends Controller
{
    public function index(DownloadCategoria $categoria)
    {
        if (! $categoria->exists) {
            $categoria = DownloadCategoria::with('downloads')->ordenados()->first() ?: \App::abort('404');
        }

        $categorias = DownloadCategoria::ordenados()->get();

        return view('frontend.downloads', compact('categorias', 'categoria'));
    }
}
