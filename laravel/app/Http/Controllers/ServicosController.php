<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Servico;

class ServicosController extends Controller
{
    public function index()
    {
        $servicos = Servico::ordenados()->get();

        return view('frontend.servicos', compact('servicos'));
    }
}
