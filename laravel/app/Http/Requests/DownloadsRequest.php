<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DownloadsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'downloads_categoria_id' => 'required',
            'titulo'                 => 'required',
            'descricao'              => 'required',
            'arquivo'                => 'required'
        ];

        if ($this->method() != 'POST') {
            $rules['arquivo'] = '';
        }

        return $rules;
    }
}
