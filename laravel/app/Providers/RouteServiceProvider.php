<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('banners', 'App\Models\Banner');
        $router->model('produtos', 'App\Models\Produto');
        $router->model('destaques', 'App\Models\Produto');
        $router->model('servicos', 'App\Models\Servico');
        $router->model('downloads', 'App\Models\Download');
        $router->model('novo', 'App\Models\Novo');
        $router->model('newsletter', 'App\Models\Newsletter');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('categorias', function($id, $route, $model = null) {
            if (str_is('*produtos*', $route->uri())) {
                $model = \App\Models\ProdutoCategoria::find($id);
            } elseif (str_is('*downloads*', $route->uri())) {
                $model = \App\Models\DownloadCategoria::find($id);
            }

            return $model ?: \App::abort('404');
        });

        $router->bind('imagens', function($id, $route, $model = null) {
            if (str_is('*novo*', $route->uri())) {
                $model = \App\Models\NovoImagem::find($id);
            } elseif ($route->hasParameter('produtos')) {
                $model = \App\Models\ProdutoImagem::find($id);
            }

            return $model ?: \App::abort('404');
        });

        $router->bind('produtos_categoria_slug', function($value) {
            return \App\Models\ProdutoCategoria::with('produtos')->slug($value)->first() ?: \App::abort('404');
        });
        $router->bind('produto_slug', function($value) {
            return \App\Models\Produto::with('imagens')->slug($value)->first() ?: \App::abort('404');
        });

        $router->bind('downloads_categoria_slug', function($value) {
            return \App\Models\DownloadCategoria::with('downloads')->slug($value)->first() ?: \App::abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
