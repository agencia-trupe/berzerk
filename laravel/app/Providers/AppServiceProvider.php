<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });

        view()->composer('frontend.*', function($view) {
            $view->with('contato', \App\Models\Contato::first());
            $view->with('destaques', \App\Models\Produto::with('categoria')->destaque()->ordenados()->limit(6)->get());
            $view->with('produtos_categorias', \App\Models\ProdutoCategoria::ordenados()->select('slug', 'titulo')->get());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
