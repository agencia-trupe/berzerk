    <footer>
        @if(Route::currentRouteName() != 'contato')
        <div class="footer-destaques">
            <div class="center">
                <h3>Destaques e Preços especiais</h3>
                <div class="destaques-thumbs">
                @foreach($destaques as $destaque)
                <a href="{{ route('produtos.show', [$destaque->categoria->slug, $destaque->slug]) }}" class="destaque">
                    <img src="{{ asset('assets/img/produtos/thumbs/'.$destaque->imagem) }}" alt="">
                    <div class="overlay">
                        <div>
                            <span>{{ $destaque->titulo }}</span>
                        </div>
                    </div>
                </a>
                @endforeach
                </div>

                @if(Route::currentRouteName() == 'home')
                <div class="chamadas">
                    <a href="{{ route('produtos.novo') }}">
                        <img src="{{ asset('assets/img/layout/hdoke.png') }}">
                    </a>
                    <a href="http://bamboole.com.br/" target="_blank">
                        <img src="{{ asset('assets/img/layout/bamboole.png') }}">
                    </a>
                </div>
                @endif
            </div>
        </div>
        @endif

        <div class="footer-links">
            <div class="center">
                <div class="categorias">
                    @foreach($produtos_categorias as $categoria)
                    <a href="{{ route('produtos.index', $categoria->slug) }}">{{ $categoria->titulo }}</a>
                    @endforeach
                </div>

                <div class="newsletter">
                    <p>Cadastre-se para receber novidades por e-mail:</p>
                    <form action="" id="form-newsletter" method="POST">
                        <input type="name" name="nome" id="newsletter_nome" placeholder="Nome" required>
                        <input type="email" name="email" id="newsletter_email" placeholder="E-mail" required>
                        <input type="submit" value="OK">
                        <div class="response-wrapper">
                            <span id="form-newsletter-response"></span>
                        </div>
                    </form>
                </div>

                <div class="redes-sociais">
                    <p>Siga:</p>
                    @if($contato->facebook) <a href="{{ $contato->facebook }}" target="_blank" class="facebook">facebook</a> @endif
                    @if($contato->googleplus) <a href="{{ $contato->googleplus }}" target="_blank" class="googleplus">googleplus</a> @endif
                    @if($contato->instagram) <a href="{{ $contato->instagram }}" target="_blank" class="instagram">instagram</a> @endif
                    @if($contato->twitter) <a href="{{ $contato->twitter }}" target="_blank" class="twitter">twitter</a> @endif
                    @if($contato->youtube) <a href="{{ $contato->youtube }}" target="_blank" class="youtube">youtube</a> @endif
                </div>

                <div class="telefone">
                    <p>Compre também pelo telefone:</p>
                    <p><span>{{ $contato->telefone }}</span></p>
                </div>

                <div class="copyright">
                    <p>{{ $contato->endereco }} | © {{ date('Y') }} {{ config('site.name') }} | Todos os direitos reservados.</p>
                    <p><a href="http://trupe.net" target="_blank">Criação de sites</a>: <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a></p>
                </div>
            </div>
        </div>
    </footer>
