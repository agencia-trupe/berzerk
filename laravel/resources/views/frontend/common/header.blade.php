    <header>
        <div class="center">
            <h1><a href="{{ route('home') }}">{{ config('site.name') }}</a></h1>

            <ul id="nav-desktop">
                <li class="has-sub" @if(str_is('produtos*',Route::currentRouteName())) class="active" @endif>
                    <a href="{{ route('produtos.index') }}">Produtos</a>
                    <ul>
                        <div class="center">
                            <li>
                                <a href="{{ route('produtos.novo') }}">NOVO!</a>
                            </li>
                            @foreach($produtos_categorias as $categoria)
                            <li>
                                <a href="{{ route('produtos.index', $categoria->slug) }}">{{ $categoria->titulo }}</a>
                            </li>
                            @endforeach
                        </div>
                    </ul>
                </li>
                <li @if(Route::currentRouteName() == 'servicos') class="active" @endif>
                    <a href="{{ route('servicos') }}">Serviços</a>
                </li>
                <li @if(Route::currentRouteName() == 'downloads') class="active" @endif>
                    <a href="{{ route('downloads') }}">Downloads</a>
                </li>
                <li @if(Route::currentRouteName() == 'contato') class="active" @endif>
                    <a href="{{ route('contato') }}">Contato</a>
                </li>
            </ul>

            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>

            <div id="busca">
                <div>
                    <form action="{{ route('busca') }}" method="GET">
                        <input type="text" name="q" value="{{ Request::get('q') }}" pattern=".{3,}" required title="Mínimo de 3 caracteres">
                        <input type="submit">
                    </form>
                </div>
            </div>
        </div>

        <div id="nav-mobile">
            <a href="{{ route('produtos.index') }}" @if(str_is('produtos*',Route::currentRouteName())) class="active" @endif>Produtos</a>
            <a href="{{ route('servicos') }}" @if(Route::currentRouteName() == 'servicos') class="active" @endif>Serviços</a>
            <a href="{{ route('downloads') }}" @if(Route::currentRouteName() == 'downloads') class="active" @endif>Downloads</a>
            <a href="{{ route('contato') }}" @if(Route::currentRouteName() == 'contato') class="active" @endif>Contato</a>
            <div id="busca-mobile">
                <div>
                    <form action="{{ route('busca') }}" method="GET">
                        <input type="text" name="q" value="{{ Request::get('q') }}" pattern=".{3,}" required title="Mínimo de 3 caracteres">
                        <input type="submit">
                    </form>
                </div>
            </div>
        </div>
    </header>
