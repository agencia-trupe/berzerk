@extends('frontend.common.template')

@section('content')

    <div class="title">
        <div class="center">
            <h2>Serviços</h2>
        </div>
    </div>

    <div class="main servicos">
        <div class="center">
            @foreach($servicos as $servico)
            <a href="{{ route('contato', ['assunto' => $servico->titulo]) }}">
                <h3>{{ $servico->titulo }}</h3>
                <h4>{{ $servico->subtitulo }}</h4>
                <p>{{ $servico->descricao }}</p>
                <span>Solicite um orçamento</span>
            </a>
            @endforeach
        </div>
    </div>

@endsection
