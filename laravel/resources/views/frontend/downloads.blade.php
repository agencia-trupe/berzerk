@extends('frontend.common.template')

@section('content')

    <div class="title">
        <div class="center">
            <h2>Downloads</h2>
        </div>
    </div>

    <div class="main downloads">
        <div class="center">
            <div class="downloads-categorias">
                @foreach($categorias as $cat)
                <a href="{{ route('downloads', $cat->slug) }}" @if($cat->id == $categoria->id) class="active" @endif>{{ $cat->titulo }}</a>
                @endforeach
            </div>

            <div class="downloads-list">
                @foreach($categoria->downloads as $download)
                <div class="download">
                    <div class="descricao">
                        <h3>{{ $download->titulo }}</h3>
                        <p>{{ $download->descricao }}</p>
                    </div>
                    <a href="{{ asset('assets/downloads/'.$download->arquivo) }}" target="_blank">Download</a>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
