@extends('frontend.common.template')

@section('content')

    <div class="title">
        <div class="center">
            <h2>Produtos</h2>
        </div>
    </div>

    <div class="main produtos">
        <div class="center">
            @include('frontend.produtos.categorias')

            <div class="produtos-main produtos-show">
                <h2>{{ $categoria->titulo }}</h2>
                <h1>{{ $produto->titulo }}</h1>

                @if(count($produto->imagens))
                <div class="produtos-imagens">
                    @foreach($produto->imagens as $imagem)
                    <img src="{{ asset('assets/img/produtos/imagens/'.$imagem->imagem) }}" alt="">
                    @endforeach
                    <div class="cycle-pager"></div>
                </div>
                @endif

                <div class="produtos-descricao">
                    {!! $produto->descricao !!}
                </div>

                @if(count($produto->downloads))
                <div class="produtos-downloads">
                    <h2>Downloads</h2>
                    @foreach($produto->downloads as $download)
                    <div class="download">
                        <div class="descricao">
                            <h3>{{ $download->titulo }}</h3>
                            <p>{{ $download->descricao }}</p>
                        </div>
                        <a href="{{ asset('assets/downloads/'.$download->arquivo) }}" target="_blank">Download</a>
                    </div>
                    @endforeach
                </div>
                @endif

                <form action="" id="form-contato" method="POST">
                    <p>Escreva-nos</p>
                    <input type="hidden" name="assunto" id="assunto" value="{{ $produto->titulo }}">
                    <input type="name" name="nome" id="nome" placeholder="Nome" required>
                    <input type="email" name="email" id="email" placeholder="E-mail" required>
                    <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required></textarea>
                    <input type="submit" value="Enviar">
                    <div class="response-wrapper">
                        <span id="form-contato-response"></span>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
