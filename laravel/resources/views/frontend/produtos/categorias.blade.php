<div class="produtos-categorias">
    <a href="{{ route('produtos.novo') }}" @if(Route::currentRouteName() == 'produtos.novo') class="active" @endif>Novo!</a>
    @foreach($produtos_categorias as $cat)
    <a href="{{ route('produtos.index', $cat->slug) }}" @if(isset($categoria) && $categoria->slug == $cat->slug) class="active" @endif>{{ $cat->titulo }}</a>
    @endforeach
</div>