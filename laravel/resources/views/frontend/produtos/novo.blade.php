@extends('frontend.common.template')

@section('content')

    <div class="title">
        <div class="center">
            <h2>Produtos</h2>
        </div>
    </div>

    <div class="main produtos">
        <div class="center">
            @include('frontend.produtos.categorias')

            <div class="produtos-main produtos-novo">
                <h2>Novo!</h2>
                <div class="produtos-novo-titulos">
                    <div>
                        <h1>{!! $novo->titulo !!}</h1>
                        <div class="subtitulo">{!! $novo->subtitulo !!}</div>
                    </div>
                    <img src="{{ asset('assets/img/novo/'.$novo->imagem) }}" alt="">
                </div>

                @if($novo->video)
                <div class="produtos-video" style="left: 0px; width: 100%; height: 0px; position: relative; padding-bottom: 56.2493%;"><iframe allowfullscreen="true" frameborder="0" mozallowfullscreen="true" src="https://www.youtube.com/embed/{{ $novo->video }}?wmode=transparent&amp;rel=0&amp;autohide=1&amp;showinfo=0&amp;enablejsapi=1" style="top: 0px; left: 0px; width: 100%; height: 100%; position: absolute;" webkitallowfullscreen="true"></iframe></div>
                @endif

                <div class="produtos-novo-right">
                    @if($novo->manual)
                    <a href="{{ asset('assets/downloads/'.$novo->manual) }}" target="_blank" class="produtos-novo-manual">Download do manual</a>
                    @endif

                    <div class="produtos-novo-imagens">
                        @foreach($imagens as $imagem)
                        <a href="{{ asset('assets/img/novo/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria"><img src="{{ asset('assets/img/novo/imagens/thumbs/'.$imagem->imagem) }}" alt=""></a>
                        @endforeach
                    </div>
                </div>

                <div class="produtos-novo-left">
                    <div class="produtos-novo-especificacoes">
                        <h3>Especificações</h3>
                        {!! $novo->especificacoes !!}
                    </div>

                    <form action="" id="form-contato" method="POST">
                        <p>Escreva-nos</p>
                        <input type="hidden" name="assunto" id="assunto" value="Novo!">
                        <input type="name" name="nome" id="nome" placeholder="Nome" required>
                        <input type="email" name="email" id="email" placeholder="E-mail" required>
                        <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required></textarea>
                        <input type="submit" value="Enviar">
                        <div class="response-wrapper">
                            <span id="form-contato-response"></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
