@extends('frontend.common.template')

@section('content')

    <div class="title">
        <div class="center">
            <h2>Produtos</h2>
        </div>
    </div>

    <div class="main produtos">
        <div class="center">
            @include('frontend.produtos.categorias')

            <div class="produtos-main produtos-list">
                <h2>{{ $categoria->titulo }}</h2>

                <div class="produtos-thumbs">
                    @foreach($produtos as $produto)
                    <a href="{{ route('produtos.show', [$produto->categoria->slug, $produto->slug]) }}">
                        <img src="{{ asset('assets/img/produtos/'.$produto->imagem) }}" alt="">
                        <span>{{ $produto->titulo }}</span>
                    </a>
                    @endforeach
                </div>

                <div class="produtos-paginacao">
                    {!! $produtos->render() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
