@extends('frontend.common.template')

@section('content')

    <div class="title">
        <div class="center">
            <h2>Contato</h2>
        </div>
    </div>

    <div class="main contato">
        <div class="center">
            <div class="info">
                <p>Compras ou informações:</p>
                <p class="telefone">{{ $contato->telefone }}</p>
                <p><a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a></p>
                <a href="http://bamboole.com.br/" class="bamboole" target="_blank">
                    <img src="{{ asset('assets/img/layout/bamboole2.png') }}">
                </a>
            </div>

            <form action="" id="form-contato" method="POST">
                <input type="hidden" name="assunto" id="assunto" value="{{ $assunto }}">
                <input type="name" name="nome" id="nome" placeholder="Nome" required>
                <input type="email" name="email" id="email" placeholder="E-mail" required>
                <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required></textarea>
                <input type="submit" value="Enviar">
                <div class="response-wrapper">
                    <span id="form-contato-response"></span>
                </div>
            </form>
        </div>

        <div class="mapa">
            {!! $contato->googlemaps !!}
        </div>
    </div>

@endsection
