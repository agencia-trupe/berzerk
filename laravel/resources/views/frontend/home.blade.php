@extends('frontend.common.template')

@section('content')

    <div class="banners-wrapper">
        <div class="banners center">
            @foreach($banners as $banner)
            <a href="{{ $banner->link }}" class="banner">
                <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="">
            </a>
            @endforeach

            <div class="cycle-pager"></div>
        </div>
    </div>

@endsection
