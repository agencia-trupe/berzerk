@extends('frontend.common.template')

@section('content')

    <div class="title">
        <div class="center">
            <h2>Resultados de busca</h2>
        </div>
    </div>

    <div class="main busca">
        <div class="center">
            @if(count($produtos))
            <div class="busca-categoria">
                <h3>Produtos:</h3>
                @foreach($produtos as $produto)
                <a href="{{ route('produtos.show', [$produto->categoria->slug, $produto->slug]) }}">{{ $produto->titulo }}</a>
                @endforeach
            </div>
            @endif

            @if(count($downloads))
            <div class="busca-categoria">
                <h3>Downloads:</h3>
                @foreach($downloads as $download)
                <a href="{{ asset('assets/downloads/'.$download->arquivo) }}" target="_blank">{{ $download->titulo }}</a>
                @endforeach
            </div>
            @endif

            @if(!count($produtos) && !count($downloads))
            <p>Sua busca não retornou nenhum resultado para <span>{{ $termo }}</span>. Tente buscar por outros termos.</p>
            @endif
        </div>
    </div>

@endsection
