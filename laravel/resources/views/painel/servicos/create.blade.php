@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Serviços /</small> Adicionar Serviço</h2>
    </legend>

    {!! Form::open(['route' => 'painel.servicos.store']) !!}

        @include('painel.servicos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
