@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Novo!
            <a href="{{ route('painel.novo.imagens.index') }}" class="btn btn-info btn-sm pull-right"><span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Editar Imagens</a>
        </h2>
    </legend>

    {!! Form::model($novo, [
        'route'  => ['painel.novo.update', $novo->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.novo.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
