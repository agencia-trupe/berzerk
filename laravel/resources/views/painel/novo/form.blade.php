@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::textarea('titulo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('subtitulo', 'Subtítulo') !!}
    {!! Form::textarea('subtitulo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($novo->imagem)
    <img src="{{ url('assets/img/novo/'.$novo->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('video', 'Vídeo (Código YouTube)') !!}
    {!! Form::text('video', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('especificacoes', 'Especificações') !!}
    {!! Form::textarea('especificacoes', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="well form-group">
    {!! Form::label('manual', 'Manual') !!}
    @if($novo->manual)
    <a href="{{ url('assets/downloads/'.$novo->manual) }}" target="_blank" style="display:block;margin:10px 0;">{{ $novo->manual }}</a>
    @endif
    {!! Form::file('manual', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
