@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('downloads_categoria_id', 'Categoria') !!}
    {!! Form::select('downloads_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('produto_id', 'Produto (opcional)') !!}
    {!! Form::select('produto_id', $produtos, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('arquivo', 'Arquivo') !!}
@if($submitText == 'Alterar' && $download->arquivo)
    <a href="{{ url('assets/downloads/'.$download->arquivo) }}" target="_blank" style="display:block;margin:10px 0;">{{ $download->arquivo }}</a>
@endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.downloads.index') }}" class="btn btn-default btn-voltar">Voltar</a>
