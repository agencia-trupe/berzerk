@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Downloads /</small> Editar Download</h2>
    </legend>

    {!! Form::model($download, [
        'route'  => ['painel.downloads.update', $download->id],
        'method' => 'patch',
        'files'  => true])
    !!}

        @include('painel.downloads.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
