@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('produtos_categoria_id', 'Categoria') !!}
    {!! Form::select('produtos_categoria_id', $categorias, '', ['class' => 'form-control select-categorias', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('produto_id', 'Produto') !!}
    {!! Form::select('produto_id', [], null, ['class' => 'form-control', 'placeholder' => 'Selecione uma Categoria']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.destaques.index') }}" class="btn btn-default btn-voltar">Voltar</a>
