@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.produtos.index') }}" class="btn btn-sm btn-default">
        &larr; Voltar para Produtos
    </a>

    <legend>
        <h2>
            <small>Produtos /</small> Destaques
            @if(count($destaques) < $limite)
            <a href="{{ route('painel.produtos.destaques.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Destaque</a>
            @endif
        </h2>
    </legend>

    @if(!count($destaques))
    <div class="alert alert-warning" role="alert">Nenhum destaque cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Categoria</th>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($destaques as $destaque)
            <tr class="tr-row">
                <td>@if($destaque->categoria){{ $destaque->categoria->titulo }}@endif</td>
                <td>{{ $destaque->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.produtos.destaques.destroy', $destaque->id), 'method' => 'delete')) !!}

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
