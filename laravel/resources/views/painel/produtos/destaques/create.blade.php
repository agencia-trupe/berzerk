@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Adicionar Destaque</h2>
    </legend>

    {!! Form::open(['route' => 'painel.produtos.destaques.store']) !!}

        @include('painel.produtos.destaques.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
