(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.bannersHome = function() {
        var $wrapper = $('.banners');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.banner',
            pagerTemplate: '<a href="#">{{slideNum}}</a>'
        });
    };

    App.envioNewsletter = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-newsletter-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/newsletter',
            data: {
                nome: $('#newsletter_nome').val(),
                email: $('#newsletter_email').val()
            },
            success: function(data) {
                $form[0].reset();
                $response.hide().text(data.message).fadeIn('slow');
            },
            error: function(data) {
                var errorMsg = data.responseJSON.nome ? data.responseJSON.nome : data.responseJSON.email;
                $response.hide().text(errorMsg).fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                assunto: $('#assunto').val(),
                nome: $('#nome').val(),
                email: $('#email').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $form[0].reset();
                $response.hide().text(data.message).fadeIn('slow');
            },
            error: function(data) {
                $response.hide().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.galeriaNovo = function() {
        var $handle = $('.fancybox');
        if (!$handle.length) return;

        $handle.fancybox({});
    };

    App.galeriaProduto = function() {
        var $wrapper = $('.produtos-imagens');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>img',
            pagerTemplate: '<a href="#">{{slideNum}}</a>'
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.bannersHome();
        this.galeriaNovo();
        this.galeriaProduto();
        $('#form-newsletter').on('submit', this.envioNewsletter);
        $('#form-contato').on('submit', this.envioContato);
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
